# openrc

Dependency based service manager (runlevel change mechanism). https://packages.debian.org/unstable/openrc

### License
* BSD 2-Clause "Simplified" License
* [gitweb.gentoo.org](https://gitweb.gentoo.org/proj/openrc.git/tree/LICENSE)
* [github.com](https://github.com/OpenRC/openrc/blob/master/LICENSE)

### Sub Packages
* openrc-dev
* openrc-doc

## Services which can be added with openrc
* dbus
  * https://gitlab.com/flatpak-packages-demo/zotero
* [postgresql](https://tracker.debian.org/postgresql-common)
  * https://gitlab.com/apt-packages-demo/postgresql
* [rsyslog](https://tracker.debian.org/rsyslog)
  * https://gitlab.com/apt-packages-demo/rsyslog
